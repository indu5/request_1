from flask import Flask,request as req
import requests
import json
from datetime import datetime,date
#from logging import FileHandler,ERROR
import logging

logging.basicConfig(filename='appp.log',level=logging.DEBUG)

app = Flask(__name__)


from datetime import date


try:

    def calculateAge(birthDate):
        days_in_year = 365.2425
        age = int((date.today() - birthDate).days / days_in_year)
        return age



    def get_random_activity():

        try:
            url = "http://www.boredapi.com/api/activity"
            logging.debug("get random activity")

            payload={}
            #headers = {}

            response = requests.request("GET", url, data=payload)

        
            response = json.loads(response.text)
            logging.debug(response['activity'])
            return response['activity'],True
        except Exception as e:
            
            return "some error occured",False



    @app.route("/",methods=["GET","POST"])
    def random_activity():
        try:
            request_body=req.get_json()
            logging.debug(request_body)
            
        
        except Exception as e:
            logging.error("Exception occurred", exc_info=True)
            
            return "invalid json"
        
    

        f = open("log.txt","w") # write to file
        f.write(str(request_body))
        
        age=0
        try:
            dob = request_body.get("DOB").split("-")
            logging.debug(dob)
            if int(dob[2])<1000:
                raise Exception("year should be four digit")
            age = calculateAge(date(int(dob[2]),int(dob[1]), int(dob[0])))
        except Exception as e:
            logging.error("Exception occurred", exc_info=True)
            logging.debug("invalid format")

            return "Invalid format"
            
            
        
    
    

        
        
        
        print(age)
        if age> 18:
            activity,status  = get_random_activity()

            if status:
                return {
                    "success" : "True",
                    "activity": activity
                }  
            else :
                return {
                    
                    "message":"invalid url"
                },400     
        
        
        else:
            return {
                "success" : "False",
                "error" : "Should be older than 18 years"
            },400
        
        
    


    # print(calculateAge(date(1997, 2, 3)))

except:

    print("Unable to process")



# if __name__=="__main__":
#     app.run(port=4500,debug=True)    